#pragma once

#include <string>
#include <deque>
#include <functional>

class Equipe
{
private:
	/// <summary>
	/// Le nom de l'�quipe
	/// </summary>
	std::string nomEquipe;

	/// <summary>
	/// Le n� de numero de l'�quipe (multiple de 100)
	/// </summary>
	int numeroEquipe;

	/// <summary>
	/// Le score (la note) obtenue par l'e concurrent'�quipe
	/// </summary>
	double scoreEquipe = 0.0;

	/// <summary>
	/// @TODO D�clarer le container pour les membres de l'�quipe avec le nom indiqu� dans le sujet (diagramme UML)
	/// Chaque membre de l��quipe est repr�sent� par une paire {dossard, score}, pair<int, int>, dans la collection.
	/// </summary>



public:
	/// <summary>
	/// Le nombre de concurrents par �quipe
	/// Attention!! Membre statique, acc�s par la classe: Equipe::tailleEquipe
	/// </summary>
	static int tailleEquipe;

	/// <summary>
	/// Contructeur
	/// </summary>
	/// <param name="nom">Le nom � affecter � l'�quipe</param>
	/// <param name="numero">Le num�ro � affecter � l'�quipe</param>
	Equipe(std::string& nom, const int& numero);

	/// <summary>
	/// Destructeur
	/// </summary>
	virtual ~Equipe();

	/// <summary>
	/// Permet de savoir si l'�quipe est compl�te
	/// </summary>
	/// <returns>true si l'�quipe est compl�te</returns>
	bool EquipeEstComplete();

	/// <summary>
	/// Accesseurs en lecture pour le nom de l'�quipe
	/// </summary>
	/// <returns>Le nom du concurrent</returns>
	std::string GetNomEquipe() { return this->nomEquipe; }
	std::string GetNomEquipe() const { return this->nomEquipe; }

	/// <summary>
	/// Accesseurs en lecture pour le numero de l'�quipe
	/// </summary>
	/// <returns>Le n� de numero du concurrent</returns>
	int GetNumeroEquipe() { return this->numeroEquipe; }
	int GetNumeroEquipe() const { return this->numeroEquipe; }

	/// <summary>
	/// Accesseurs en lecture pour le score de l'�quipe
	/// </summary>
	/// <returns>Le score du concurrent</returns>
	double GetScoreEquipe() { return this->scoreEquipe; }
	double GetScoreEquipe() const { return this->scoreEquipe; }

	/// <summary>
	/// Ajoute un membre � l'�quipe, si elle n'est pas compl�te.
	/// </summary>
	/// <param name="membre">Le dossard du membre � ajouter</param>
	/// <returns>true s'il reste de la place</returns>
	bool AjouterMembre(int dossard);
};

