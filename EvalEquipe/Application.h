#pragma once

#include "Menu.h"
#include "Concurrent.h"
#include "Equipe.h"
#include <list>
#include <deque>
#include <map>
#include <queue>
#include <functional>
#include <iterator>
#include <vector>

using namespace std;

/// <summary>
/// La classe Application est celle qui contient tout le fonctionnement du programme
/// </summary>
class Application
{
private:
	/// <summary>
	/// Le menu de l'application
	/// </summary>
	Menu menu;

	/// <summary>
	/// La taille des �quipes (2 membres par d�faut)
	/// </summary>
	int tailleDesEquipes = 2;

	/// <summary>
	/// Le nombre d'�quipes (2 par d�faut)
	/// </summary>
	int nombreEquipes = 2;

	/// <summary>
	/// @TODO D�clarer les conteneurs avec les noms et types indiqu�s dans le sujet
	/// </summary>







public:
	/// <summary>
	/// Initialiser la comp�tition.
	/// Consiste � :
	/// 1) saisir le nombre d'�quipe pouvant participer � la comp�tition
	/// 2) g�n�rer les �l�ments de la collection numerosDesEquipes (1000, 2000, ...)
	/// 3) saisir le nombre de concurrents max par �quipe
	/// 4) g�n�rer les �l�ments de la collection dossardsPourAffecterAuxConcurrents (1, 2, 3, ...)
	/// </summary>
	void InitialiserCompetition();

	/// <summary>
	/// Inscription d'une �quipe
	/// Consiste � :
	/// 1) saisir le nom de l'�quipe
	/// 2) choisir le premier �l�ment dans la collection numerosDesEquipes
	/// 3) cr�er l'�quipe dans la collection lesEquipes avec le nom saisi et le num�ro extrait 
	/// 4) supprimer le num�ro extrait de la collection numerosDesEquipes
	/// Pas de saisie possible s'il n'y a plus de place disponnible.
	/// Affiche l'�quipe et le nombre de places restantes.
	/// </summary>
	void InscrireUneEquipe();

	/// <summary>
	/// Affiche la liste des �quipes inscrites, par ordre croissant des num�ros d'�quipe
	/// </summary>
	void AfficherLesEquipesParNumero();

	/// <summary>
	/// Affiche la liste des �quipes inscrites, par ordre croissant des noms d'�quipe
	/// </summary>
	void AfficherLesEquipesParNom();

	/// <summary>
	/// Inscrire un concurent.
	/// Consiste �:
	/// 1) choisir l'�quipe dans laquelle inscrire le concurrent
	/// 2) saisir le nom du concurrent
	/// 3) choisir et retirer 1 num�roConcurrent au hasard dans la collection dossardsPourAffecterAuxConcurrents
	/// 4) cr�er le concurrent avec son nom et le dossard d�finitif (num�roEquipe + num�roConcurrent) dans la collection concurentsInscrits.
	/// 5) ajouter le dossard du concurrent aux membres de l'�quipe
	/// L'�quipe doit exister et ne pas �tre compl�te.
	/// Pas d'inscription possible s'il ne reste plus de dossard disponible.
	/// </summary>
	void InscrireUnConcurrent();

	/// <summary>
	/// Affiche la liste des concurrents inscrits, par ordre croissant des dossards
	/// </summary>
	void AfficherLesConcurrentsParDossard();

	/// <summary>
	/// Permet de noter (scorer) tous les concurrents.
	/// Consiste �:
	/// 1) affecter un score entier entre 0 et 10 inclus � chaque concurrent inscrit.
	/// 2) ins�re un �l�ment <score, dossard> dans la collection resultatsIndividuels, les mieux not�s rang�s en premier
	/// 3) calcule les scores des �quipes.
	/// Le score sert de cl�.
	/// Attention!! On peut avoir plusieurs concurrents avec le m�me score.
	/// Attention!! Un concurrent n'est not� qu'une seule fois.
	/// </summary>
	void NoterConcurrents();

	/// <summary>
	/// Affiche le score, le dossard et le nom des concurrents not�s.
	/// </summary>
	void AfficherResultatsIndividuels();

	/// <summary>
	/// Boucle d'ex�cution du programme.
	/// </summary>
	void Run();
};

