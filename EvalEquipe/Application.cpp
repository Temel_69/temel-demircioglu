#include <iostream>
#include <Windows.h>
#include <algorithm>
#include "Application.h"
#include "Concurrent.h"

using namespace std;

/// <summary>
/// Retourne n, un nombre entier g�n�r� au hasard,
/// avec deb <= n < fin
/// </summary>
/// <param name="deb"> la valeur minimale de n</param>
/// <param name="fin"> la valeur � laquelle n doit �tre < </param>
/// <returns> n avec deb <= n < fin </returns>
int hasard(int deb, int fin)
{
	int val;
	val = (rand() % fin) + deb;
	return val;
}

/// <summary>
/// Initialiser la comp�tition.
/// Consiste � :
/// 1) saisir le nombre d'�quipe pouvant participer � la comp�tition
/// 2) g�n�rer les �l�ments de la collection numerosDesEquipes (1000, 2000, ...)
/// 3) saisir le nombre de concurrents max par �quipe
/// 4) g�n�rer les �l�ments de la collection dossardsPourAffecterAuxConcurrents (1, 2, 3, ...)
/// </summary>
void Application::InitialiserCompetition()
{
	cout << "-----------------------------------" << endl;
	cout << "-           Initialisation        -" << endl;
	cout << "-----------------------------------" << endl;

	cout << endl << "Saisissez le nombre de membres par �quipe (de 2 � 10)" << endl;
	this->tailleDesEquipes = menu.SaisirEntierEntre(2, 10);

	cout << endl << "Saisissez le nombre d'�quipes � cr�er (de 2 � 10)" << endl;
	this->nombreEquipes = menu.SaisirEntierEntre(2, 10);

	cout << endl << "--> TODO:  G�n�rer les n� et remplir numerosDesEquipes" << endl << endl;

}

/// <summary>
/// Inscription d'une �quipe
/// Consiste � :
/// 1) saisir le nom de l'�quipe
/// 2) choisir le premier �l�ment dans la collection numerosDesEquipes
/// 3) cr�er l'�quipe dans la collection lesEquipes avec le nom saisi et le num�ro extrait 
/// 4) supprimer le num�ro extrait de la collection numerosDesEquipes
/// Pas de saisie possible s'il n'y a plus de place disponnible.
/// Affiche l'�quipe et le nombre de places restantes.
/// </summary>
void Application::InscrireUneEquipe()
{
	cout << "--> TODO: D�velopper Application::InscrireUneEquipe()" << endl;

}

/// <summary>
/// Affiche la liste des �quipes inscrites, par ordre croissant des num�ros d'�quipe
/// </summary>
void Application::AfficherLesEquipesParNumero()
{
	cout << "--> TODO: D�velopper Application::AfficherLesEquipesParNumero()" << endl;
}

/// <summary>
/// Affiche la liste des �quipes inscrites, par ordre croissant des noms d'�quipe
/// </summary>
void Application::AfficherLesEquipesParNom()
{
	cout << "--> TODO: D�velopper Application::AfficherLesEquipesParNom()" << endl;
}

/// <summary>
/// Inscrire un concurent.
/// Consiste �:
/// 1) choisir l'�quipe dans laquelle inscrire le concurrent
/// 2) saisir le nom du concurrent
/// 3) choisir et retirer 1 num�roConcurrent au hasard dans la collection dossardsPourAffecterAuxConcurrents
/// 4) cr�er le concurrent avec son nom et le dossard d�finitif (num�roEquipe + num�roConcurrent) dans la collection concurentsInscrits.
/// 5) ajouter le dossard du concurrent aux membres de l'�quipe
/// L'�quipe doit exister et ne pas �tre compl�te.
/// Pas d'inscription possible s'il ne reste plus de dossard disponible.
/// </summary>
void Application::InscrireUnConcurrent()
{
	cout << "--> TODO: D�velopper Application::InscrireUnConcurrent()" << endl;
}

/// <summary>
/// Affiche la liste des concurrents inscrits, par ordre croissant des dossards
/// </summary>
void Application::AfficherLesConcurrentsParDossard()
{
	cout << "--> TODO: D�velopper Application::AfficherLesConcurrentsParDossard()" << endl;
}

/// <summary>
/// Permet de noter (scorer) tous les concurrents.
/// Consiste �:
/// 1) affecter un score entier entre 0 et 10 inclus � chaque concurrent inscrit.
/// 2) ins�re un �l�ment <score, dossard> dans la collection resultatsIndividuels, les mieux not�s rang�s en premier
/// 3) calcule les scores des �quipes.
/// Le score sert de cl�.
/// Attention!! On peut avoir plusieurs concurrents avec le m�me score.
/// Attention!! Un concurrent n'est not� qu'une seule fois.
/// </summary>
void Application::NoterConcurrents()
{
	cout << "--> TODO: D�velopper Application::NoterConcurrents()" << endl;
}

/// <summary>
/// Affiche le score, le dossard et le nom des concurrents not�s.
/// </summary>
void Application::AfficherResultatsIndividuels()
{
	cout << "--> TODO: D�velopper Application::AfficherResultatsIndividuels()" << endl;
}

/// <summary>
/// Boucle d'ex�cution du programme.
/// </summary>
void Application::Run()
{
	bool quit = false;
	int choix = 0;

	do
	{
		//  Affiche le menu et lit le choix de l'utilisateur
		menu.Affiche();
		choix = menu.SaisirEntierEntre(1, 7);

		switch(choix)	//  R�alise l'action choisie par l'utilisateur
		{
		case 1:
			InscrireUneEquipe();
			break;
		case 2:
			AfficherLesEquipesParNom();
			break;
		case 3:
			InscrireUnConcurrent();
			break;
		case 4:
			AfficherLesConcurrentsParDossard();
			break;
		case 5:
			NoterConcurrents();
			break;
		case 6:
			AfficherResultatsIndividuels();
			break;
		case 7:
			quit = true;
			break;
		}
	} while (!quit);
}
